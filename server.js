/*
var static = require('node-static'),
  http = require('http'),
  util = require('util');

var webroot = './public',
	port = 80,
	restport = 81;

var file = new(static.Server)(webroot, { 
	cache: 600, 
	headers: {'Access-Control-Allow-Origin': '*' } 
});

http.createServer(function(req, res) {
}).listen(port, '2003:6a:6d0f:7401:e1cd:3493:8c53:3c70');
*/

var static = require('node-static');
var webroot = './public';
var file = new(static.Server)(webroot, { 
	cache: 600, 
	headers: {'Access-Control-Allow-Origin': '*' } 
});

var express = require('express');
var app = express();
app.use(express.bodyParser());
app.use(express.logger());
app.use(function(req, res, next){
	if(false)	{
		
		  res.writeHead(200, {'content-type': 'text/plain'});
		  res.end('test');
	}
	else	{
		file.serve(req, res, function(err) {
			if(err) next();
		});
	}
});

var ships = new Array(5);

app.get('/:id/ships', function(req, res){
	//console.log(req.url, req.headers.host);

	var id = parseInt(req.params.id);
	var content = [];
	for(var i = 0, l = ships.length; i < l; i++)	{
		if(i != id) content.push(ships[i]);
	}
	res.setHeader('Content-Type', 'application/json');
	res.end(JSON.stringify(content));
});

app.post('/:id/ships', function(req, res){
	var id = parseInt(req.params.id);
	ships[id] = JSON.parse(req.body.content);
	res.end('');
});


app.listen(80, '2003:6a:6d0f:7401:e1cd:3493:8c53:3c70');
