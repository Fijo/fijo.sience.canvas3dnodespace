var canvas = document.getElementById('canvas');
canvas.setAttribute('width', screen.width);
canvas.setAttribute('height', screen.height);


var clientid = prompt('your id? (0-4)');


var netShips = new (function()	{
	var otherShips = [];
	var myShip = null;
	var reciveOtherShips = function()	{
		$.ajax({
			url: '/'+clientid+'/ships',
			type: 'get',
			async: true,
			success: function(res)	{
				otherShips = res;
			}
		});
	};
	setInterval(reciveOtherShips, 120);


	var updateMyShip = function()	{
		if(myShip == null) return;
		$.ajax({
			url: '/'+clientid+'/ships',
			type: 'post',
			data: {content: JSON.stringify(myShip)},
			async: true,
			success: function()	{
			}
		});
	};
	setInterval(updateMyShip, 120);

	this.send = function(data)	{
		myShip = data;
	};

	this.get = function()	{
		return otherShips;
	};

})();



var movementManager = new (function()	{
	var self = this;
	self.listeners = [];

	var movement = {x: 0, y: 0, speed: 0};

	var keysDown = [];

	setInterval(function(e)	{
		for(var i = 0, l = keysDown.length; i < l; i++)	{
			var keyCode = keysDown[i];
			var movementChange = {x: 0, y: 0, speed: 0};
			switch(keyCode)	{
				case 83://s
					movementChange.y--;
					break;
				case 87://w
					movementChange.y++;
					break;
				case 65://a
					movementChange.x--;
					break;
				case 68://d
					movementChange.x++;
					break;
				case 89://y
					movementChange.speed--;
					break;
				case 88://x
					movementChange.speed++;
					break;
				default:
					continue;
			}
			movement.x += movementChange.x;
			movement.y += movementChange.y;
			movement.speed += movementChange.speed;
			for(var i = 0, l = self.listeners.length; i < l; i++)
				self.listeners[i](movement, movementChange);
		}
	});

	document.onkeydown = function(e)	{
		var keyCode = e.keyCode;
		if(keysDown.indexOf(keyCode) == -1)
			keysDown.push(keyCode);
	};

	document.onkeyup = function(e)	{
		var keyCode = e.keyCode;
		var index = keysDown.indexOf(keyCode);
		if(index != -1)
			keysDown.splice(index, 1);
	};
})();

var cam = new c3dl.FreeCamera();
var spaceship = function(movementManager, netShips)	{
	var self = this;

	var movementChanges = {x: 0, y: 0};
	var newMovement = {x: 0, y: 0, speed: 0};
	var linearVel = Array(0, 0, 0);
	movementManager.listeners.push(function(movement, movementChange)	{
		newMovement = movement;
		movementChanges.x += movementChange.x;
		movementChanges.y += movementChange.y;
	});

	self.models = {
		main: "models/SpaceShip/SpaceShip.dae"
	};
	self.init = function()	{
		var model = new c3dl.Collada();
		model.init(self.models.main);

		cam.setPosition(new Array(0.0, 0.0, 0.0));

		self.model = model;
	};
	self.update = function()	{

		self.model.yaw(movementChanges.x / 50);
		self.model.pitch(movementChanges.y / 50);

		var position = self.model.getPosition();
		var direction = self.model.getDirection();
		netShips.send({position: position, direction: direction});

		var speedFactor = newMovement.speed / 200;
		linearVel = new Array(direction[0]*speedFactor, direction[1]*speedFactor, direction[2]*speedFactor);
		self.model.setLinearVel(linearVel);

		cam.setPosition(new Array(position[0]-(direction[0]*15), position[1]-(direction[1]*15), position[2]-(direction[2]*15)));
		cam.setLookAtPoint(new Array(direction[0]+position[0], direction[1]+position[1], direction[2]+position[2]));
		cam.setLinearVel(linearVel);

		movementChanges.x = 0;
		movementChanges.y = 0;
		movementChanges.speed = 0;

	};
};



var spaceshipOfOthers = function(index, netShips)	{
	var self = this;

	self.models = {
		main: "models/SpaceShip/SpaceShip.dae"
	};
	self.init = function()	{
		var model = new c3dl.Collada();
		model.init(self.models.main);

		model.setPosition(new Array(0.0, 0.0, 0.0));

		self.model = model;
	};
	self.update = function()	{
		var netShip = netShips.get()[index];
		if(netShip == null) return;
		
		self.model.setPosition(netShip.position);
		//self.model.setDirection(netShip.direction);
	};
};


var elementPrototypeFactory = function(colladaModel, texture)	{
	return function()	{
		var self = this;
		self.models = {
			main: colladaModel
		};
		self.init = function()	{
			var model = new c3dl.Collada();
			model.init(self.models.main);
			model.setTexture(texture);

			model.setPosition(new Array((0.5-Math.random())*10000, (0.5-Math.random())*10000, (0.5-Math.random())*10000));
			model.setLinearVel(new Array((0.5-Math.random())/1000, (0.5-Math.random())/1000, (0.5-Math.random())/1000));
			model.setAngularVel(new Array((0.5-Math.random())/100, (0.5-Math.random())/100, (0.5-Math.random())/100));

			self.model = model;
		};
	};
};

var elementPrototypes = {
/*	asteroid:	{
		factory: elementPrototypeFactory('models/Rock1/Rock1.dae', 'models/Rock1/Rock-Texture-Surface.png'),
		count: 1
	},*/
	alienPlanet:	{
		factory: elementPrototypeFactory('models/AlienPlanet/AlienPlanet.dae', 'models/AlienPlanet/planet_Bog1200.png'),
		count: 500
	},
	marsPlanet:	{
		factory: elementPrototypeFactory('models/MarsPlanet/MarsPlanet.dae', 'models/MarsPlanet/MarsMap.png'),
		count: 400
	}
};

var elements = [
	new spaceship(movementManager, netShips),
	new spaceshipOfOthers(0, netShips),
	new spaceshipOfOthers(1, netShips),
	new spaceshipOfOthers(2, netShips),
	new spaceshipOfOthers(3, netShips),
	new spaceshipOfOthers(4, netShips)
];

for(var key in elementPrototypes)	{
	var elementPrototype = elementPrototypes[key];
	for(var i = 0, l = elementPrototype.count; i < l; i++)
		elements.push(new elementPrototype.factory());
}

var testComponent = function(elements, cam)	{
	this.models = {
		init: function()	{
			var allModels = {};
			for(var i = 0, l = elements.length; i < l; i++)	{
				var models = elements[i].models;
				for(var key in models)
					allModels[models[key]] = true;
			}
			for(var model in allModels)
				c3dl.addModel(model);
		}
	};

	this.scenes = new (function()	{
		this.elements = elements;
		this.cam = cam;

		this.init = function(scn)	{
			this.scn = scn;

			scn.setBackgroundColor([0,0,0]);

			for(var i = 0, l = this.elements.length; i < l; i++)	{
				var element = this.elements[i];
				element.init();
				scn.addObjectToScene(element.model);
			}
		
			scn.setCamera(this.cam);
		};
		this.update = function()	{
			for(var i = 0, l = this.elements.length; i < l; i++)	{
				var element = this.elements[i];
				if(element.update == null) break;
				element.update();
			}			
		};
	})();
};


var canvasControler = function(elementId, component)	{
	var initCanvas = function(canvasName)	{	
		var scn = new c3dl.Scene();
		scn.setCanvasTag(canvasName);

		renderer = new c3dl.WebGL();
		renderer.createRenderer(this);

		scn.setRenderer(renderer);
		scn.init(canvasName);

		if(!renderer.isReady()) throw 'Error: renderer not ready yet ?!';
	
		component.scenes.init(scn);
		scn.setUpdateCallback(component.scenes.update);
		scn.startScene();
	};

	component.models.init();
	c3dl.addMainCallBack(initCanvas, elementId);
};

new canvasControler('canvas', new testComponent(elements, cam));
